﻿using Moq;
using NUnit.Framework;
using StackExchange.StacMan;
using StackExchangeClient;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace StackExchangeClientTests
{
    [TestFixture]
    public class QuestionsStatsCalculatorTests
    {
        [Test(Description = "Unit test - mocking a failed response to check behaviour.")]
        public void GetStatsByDate_ResponseUnsuccessful_ThrowsException()
        {
            //Arrange the response to be unsuccessful
            var responseMock = new StacManResponse<Question>();
            responseMock.SetProperty(r => r.Success, false);
            //Arrange the response to be returned
            var questionsMethodsMock = new Mock<IQuestionMethods>();
            questionsMethodsMock.Setup(qm => qm.GetAll(
                It.IsAny<string>(), null, It.IsAny<int>(), 
                It.IsAny<int>(), null, null, null, 
                It.IsAny<DateTime>(), It.IsAny<DateTime>(), null, null, null, null)
            ).Returns(Task.FromResult(responseMock));

            QuestionsStatsCalculator calc = new QuestionsStatsCalculator(questionsMethodsMock.Object);

            Assert.Throws<Exception>(() => calc.GetStatsByDate(DateTime.Now, DateTime.Now.AddMinutes(1)));
            questionsMethodsMock.VerifyAll();
        }

        [Test(Description = "Integration test - hard coded values from the past for regression testing.")]
        public void GetStatsByDate_OnePage_ReturnsValidStats()
        {
            QuestionsStatsCalculator calc = new QuestionsStatsCalculator(new StacManClient());
            var stats = calc.GetStatsByDate(DateTime.ParseExact("20170501 00:00", "yyyyMMdd HH:mm", CultureInfo.InvariantCulture), DateTime.ParseExact("20170501 00:05", "yyyyMMdd HH:mm", CultureInfo.InvariantCulture));
            Assert.AreEqual(7, stats.QuestionsCount);
            Assert.AreEqual(12639, stats.TotalViewsCount);
            Assert.AreEqual(17, stats.AllTags.ToList().Count());
        }

        [Test(Description = "Integration test - hard coded values from the past for regression testing.")]
        public void GetStatsByDate_ManyPages_ReturnsValidStats()
        {
            QuestionsStatsCalculator calc = new QuestionsStatsCalculator(new StacManClient());
            var stats = calc.GetStatsByDate(DateTime.ParseExact("20170501 00:00", "yyyyMMdd HH:mm", CultureInfo.InvariantCulture), DateTime.ParseExact("20170501 01:00", "yyyyMMdd HH:mm", CultureInfo.InvariantCulture));
            Assert.AreEqual(154, stats.QuestionsCount);
            Assert.AreEqual(166796, stats.TotalViewsCount);
            Assert.AreEqual(268, stats.AllTags.ToList().Count());
        }
    }
}
