﻿using StackExchange.StacMan;
using StackExchangeClient;
using System;
using System.Globalization;
using System.Linq;

namespace StackExchangeApp
{
    class Program
    {
        private static IQuestionsStatsCalculator questionsRepository;

        static void Main(string[] args)
        {
            Initialize();
            Console.WriteLine("Please provide the input date in yyyy-MM-dd format.");
            var inputStr = Console.ReadLine();

            DateTime inputDate;
            if (!DateTime.TryParseExact(inputStr, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out inputDate))
            {
                Console.WriteLine("The input you provided is not in the right format.");
                Console.ReadLine();
                return;
            }

            var stats = questionsRepository.GetStatsByDate(inputDate, inputDate.AddMinutes(1));
            Console.WriteLine("There was a total of " + stats.QuestionsCount + " questions asked on this day.");
            Console.WriteLine("There was a total of " + stats.TotalViewsCount + " views across these questions.");
            Console.WriteLine("Here's a list of all tags used for these questions:");
            stats.AllTags.ToList().ForEach(t => Console.WriteLine(t));
            Console.ReadLine();
        }

        private static void Initialize()
        {
            questionsRepository = new QuestionsStatsCalculator(new StacManClient());     
        }
    }
}
