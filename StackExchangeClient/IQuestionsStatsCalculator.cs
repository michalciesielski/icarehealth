﻿using System;

namespace StackExchangeClient
{
    public interface IQuestionsStatsCalculator
    {
        QuestionsStats GetStatsByDate(DateTime fromDate, DateTime toDate);
    }
}
