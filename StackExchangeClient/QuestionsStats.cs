﻿using System.Collections.Generic;

namespace StackExchangeClient
{
    public class QuestionsStats
    {
        public QuestionsStats()
        {
            AllTags = new List<string>();
        }

        public int QuestionsCount { get; set; }

        public int TotalViewsCount { get; set; }

        public IEnumerable<string> AllTags { get; set; }
    }
}
