﻿using System;
using StackExchange.StacMan;
using System.Linq;

namespace StackExchangeClient
{
    public class QuestionsStatsCalculator : IQuestionsStatsCalculator
    {
        private IQuestionMethods questionMethods;

        public QuestionsStatsCalculator(IQuestionMethods questionMethods)
        {
            this.questionMethods = questionMethods;
        }

        public QuestionsStats GetStatsByDate(DateTime fromDate, DateTime toDate)
        {
            bool hasMore = true;
            int pageNum = 1;
            var result = new QuestionsStats();

            while (hasMore)
            {
                var response = questionMethods.GetAll("stackoverflow", mindate: fromDate, maxdate: toDate, page: pageNum, pagesize: 100).Result;
                if (!response.Success)
                {
                    throw new Exception("Could not get a response from StackExchange.", response.Error);
                }

                result.QuestionsCount += response.Data.Items.Count();
                result.TotalViewsCount += response.Data.Items.Sum(q => q.ViewCount);
                result.AllTags = Enumerable.Union(result.AllTags, response.Data.Items.SelectMany(q => q.Tags));
                pageNum++;
                hasMore = response.Data.HasMore;
            }

            return result;
        }
    }
}
